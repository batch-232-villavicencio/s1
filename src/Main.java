public class Main {
    // Main Class
        // entry point for our java program
        // main class has 1 method inside, the main method => to run our code
    public static void main(String[] args) {
        /*
            "public" - access modifier which simply tells the application which classes have access to our method/attributes
            "static" - method / property that belongs to the class. It is accessible without having to create an instance of an object
            "void" - the method will not return any data. Because in java we have to declare the data type of the method's return
        */
        System.out.println("Hello world!");
        System.out.println("Christine Garcia");
    }
}