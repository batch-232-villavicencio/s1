package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        // variable

        int age;
        char middle_name;

        // variable initialization
        int x = 0;
        age = 18;

        // L is added at the end of the long number to be recognized as long. Otherwise, it will wrongfully recognize at int
        long worldPopulation = 7862881145L;
        System.out.println(worldPopulation);

        // float - we have to add f at the end of a float to be recognized
        float piFloat = 3.14159f;
        System.out.println(piFloat);

        // double
        double piDouble = 3.1415926;
        System.out.println(piDouble);

        // char can only hold 1 character
        // uses single quotes
        char letter = 'a';

        // boolean
        boolean isMVP = true;
        boolean isChampion =  false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        // constants in Java
        // final datatype VARIABLENAME
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        // String - non-primitive data type. String are actually object than can use methods
        String username = "theTinker23";
        System.out.println(username);

        // isEmpty is a string method which returns a boolean
        // checks the length of the string, returns true of the length is 0, returns false otherwise
        System.out.println(username.isEmpty()); //result:false



    }
}
