package com.zuitt.example;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Activity {

    private static final DecimalFormat df = new DecimalFormat("0.00");

    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);
        System.out.println("First Name:");
        String name = myObj.nextLine();
        System.out.println("Last Name:");
        name = name + " " + myObj.nextLine();
        System.out.println("First Subject Grade:");
        double gradeAverage = myObj.nextDouble();
        System.out.println("Second Subject Grade:");
        gradeAverage = gradeAverage + myObj.nextDouble();
        System.out.println("Third Subject Grade:");
        gradeAverage = gradeAverage + myObj.nextDouble();
        System.out.println("Good day, " + name);
        System.out.print("Your grade average is: " + df.format(gradeAverage/3));
    }
}
